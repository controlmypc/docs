# controlmypc developers documentation

https://cmpc.live

Hello, thanks for checking out the developer documentation. If you're seeing this you're a developer (duh) so you should understand basic development terms.

**The documentation has moved to the Wiki of this repository.**

[Wiki](https://gitlab.com/controlmypc/docs/-/wikis/home)

- [Website](https://gitlab.com/controlmypc/docs/-/wikis/documentation/Website)
- [Script](https://gitlab.com/controlmypc/docs/-/wikis/documentation/Script)
- [Twitch bot](https://gitlab.com/controlmypc/docs/-/wikis/documentation/Twitch-bot)
- [Discord bots](https://gitlab.com/controlmypc/docs/-/wikis/documentation/Discord-bots)

[Other info](https://gitlab.com/controlmypc/docs/-/wikis/Other-info)

[Contributing](https://gitlab.com/controlmypc/docs/-/wikis/wiki-meta/CONTRIBUTING)
